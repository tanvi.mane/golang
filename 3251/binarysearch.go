package main

import "fmt"

func bs(number int, arr []int) bool {

	low := 0
	high := len(arr) - 1

	for low <= high {
		m := (low + high) / 2

		if arr[m] < number {
			low = m + 1
		} else {
			high = m - 1
		}
	}

	if low == len(arr) || arr[low] != number {
		return false
	}

	return true
}

func main() {
	arr := []int{1, 5, 10, 34, 45, 59, 68, 70, 77, 84, 96}
	fmt.Println(bs(5, arr))
	fmt.Println(bs(12, arr))
}
