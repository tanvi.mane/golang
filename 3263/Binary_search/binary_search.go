// Write the program to do the binary search on an array
package main

import "fmt"

func binarySearch(arr []int, search int) int {
	low := 0
	high := len(arr) - 1

	for low <= high {
		mid := (low + high) / 2

		if arr[mid] == search {
			return mid
		} else if arr[mid] < search {
			low = mid + 1
		} else {
			high = mid - 1
		}
	}
	return -1
}
func main() {
	arr := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	search := 5

	index := binarySearch(arr, search)

	if index == -1 {
		fmt.Println("Element not found")
	} else {
		fmt.Printf("Element found at index %d\n", index)
	}
}
