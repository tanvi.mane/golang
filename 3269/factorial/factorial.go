package main

import "fmt"

var factorial int

func Fact(n int) int {
    factorial = 1
    for i := 1; i <= n; i++ {
        factorial = factorial * i
    }
    return factorial
}
func main() {

    var n int

    fmt.Print("Enter any Number to find the Factorial = ")
    fmt.Scanln(n)

    factorial = Fact(n)
    fmt.Println("The Factorial of ", n, " = ", factorial)
}