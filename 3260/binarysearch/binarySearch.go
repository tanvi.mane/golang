// find any element in the array using binary search algorithm
package main

import "fmt"

func binarySearch(num int, array []int) bool {

	low := 0
	high := len(array) - 1

	for low <= high {
		mid := (low + high) / 2

		if array[mid] < num {
			low = mid + 1
		} else {
			high = mid - 1
		}
	}

	if low == len(array) || array[low] != num {
		return false
	} else {
		return true
	}
}

func main() {
	arr := []int{1, 2, 3, 5, 6, 10, 11, 16, 17, 26, 36, 42}
	fmt.Println(binarySearch(26, arr))
	fmt.Println(binarySearch(4, arr))
}
