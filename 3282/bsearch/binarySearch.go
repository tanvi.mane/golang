/*

 Binary search is a logarithmic search algorithm 
 that finds the position of a target value within a sorted array. 
 Ths algorithm compares the target value 
 to the middle element of the array.

 Average Time Complexity: O(log n) where n is the length of array
*/

package main

import( 
	"fmt"
	"sort"
)

func binarySearch(arr []int,targetElm int, low int, high int) int{
	if(low>high) {
		return -1
	}

	var mid int=low+(high-low)/2

	if arr[mid]<targetElm {
		return binarySearch(arr,targetElm,mid+1,high)
	} else if arr[mid]>targetElm {
		return binarySearch(arr,targetElm,low,mid-1)
	} else {
		return mid
	}
		
}

func main(){
	var n int
	fmt.Print("Enter size of array: ")
	fmt.Scanln(&n)
	arr:=[]int{}
	for i:=0;i<n;i++ {
		var elm int
		fmt.Printf("Enter element %d: ",i)
		fmt.Scanln(&elm)
		arr=append(arr,elm)
	}

	var targetElm int
	fmt.Print("Enter target element: ")
	fmt.Scanln(&targetElm)

	sort.Slice(arr, func(i int, j int) bool{
		return arr[i]<arr[j]
	})
	fmt.Print(arr)
	var index int=binarySearch(arr,targetElm,0,n-1)

	fmt.Println("Element found at ",index)

}