// Binary Search implementation in GO
package main

import "fmt"

func binarySearch(arr []int, target int) int {
	low, high := 0, len(arr)-1

	for low <= high {
		mid := (low + high) / 2

		if arr[mid] == target {
			return mid
		} else if arr[mid] < target {
			low = mid + 1
		} else {
			high = mid - 1
		}
	}

	return -1
}

func main() {
	arr2 := []int{4, 5, 6, 7, 148, 23}
	target := 5

	ans := binarySearch(arr2, target)

	if ans == -1 {
		fmt.Printf("%d Did not found any array\n", ans)
	} else {
		fmt.Printf("%d Found at the index at %d", target, ans)
	}

}
